import { bindable, bindingMode } from "aurelia-framework";
import { RecognitionUnit } from "../ink-recognition-service/RecognitionUnit";
import { Recognition } from "../ink-recognition-service/Recognition";
import { RecognitionAlternative } from "../ink-recognition-service/RecognitionAlternative";
import { InkRecognizerResult } from "../ink-recognition-service/InkRecognizerResult";

@bindable({ name: 'recognitions', attribute: 'recognitions', defaultBindingMode: bindingMode.oneWay })
export class InkRecognitionList {
  public recognitions: InkRecognizerResult;

  constructor() {

  }


  public findRecognizedLines = (recognitionResult: InkRecognizerResult): string[] => {
    return !!recognitionResult && !!recognitionResult.recognitionUnits
      ? recognitionResult.recognitionUnits
        .filter(u => u.category === "line") // TODO: enum
        .map(u => u.recognizedText)
      : [];
  }
}
