/**
 * @see https://docs.microsoft.com/en-us/rest/api/cognitiveservices/inkrecognizer/inkrecognizer
 */
export interface Recognition {
  category: string;
  recognizedText: string;
}
