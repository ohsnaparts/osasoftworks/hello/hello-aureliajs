import { Point } from "paper";
import { InkRecognizerResult } from "./InkRecognizerResult";

export default class InkDetectionService {

  public async recognizeInk(paths: Point[][], apiUrl: string, subscriptionKey: string): Promise<InkRecognizerResult> {
    const pointsToString = (points: Point[]) => points.map(p => `${p.x},${p.y}`).join(',');

    let counter = 0;
    const language = "en-US";
    const strokes = paths.map(pointsToString).map(points => (
      { id: counter++, points, language }
    ));

    const body = JSON.stringify({
      version: 1,
      language: "en-US",
      unit: "mm",
      strokes: strokes
    });

    return await fetch(apiUrl, {
      headers: [
        ["Ocp-Apim-Subscription-Key", subscriptionKey],
        ["content-type", "application/json"]
      ],
      body,
      method: "PUT"
    }).then(response => response.json())
      .then(response => {
        console.log(response);
        return <InkRecognizerResult> response;
      });
  };

}
