import { bindingMode, bindable } from "aurelia-framework";

@bindable({ name: 'url', attribute: 'url', defaultBindingMode: bindingMode.twoWay })
@bindable({ name: 'key', attribute: 'key', defaultBindingMode: bindingMode.twoWay })
export class InkRecognitionForm {
  private url: string = ''
  private key: string = ''

  constructor() {

  }
}
