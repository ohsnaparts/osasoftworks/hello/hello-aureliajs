export default {
  "recognitionUnits": [
    {
      "alternates": [
        {
          "category": "inkWord",
          "recognizedString": "Hey"
        },
        {
          "category": "inkWord",
          "recognizedString": "they"
        },
        {
          "category": "inkWord",
          "recognizedString": "Thy"
        },
        {
          "category": "inkWord",
          "recognizedString": "fly"
        },
        {
          "category": "inkWord",
          "recognizedString": "Hay"
        },
        {
          "category": "inkWord",
          "recognizedString": "Gley"
        },
        {
          "category": "inkWord",
          "recognizedString": "thy"
        },
        {
          "category": "inkWord",
          "recognizedString": "Hwy"
        },
        {
          "category": "inkWord",
          "recognizedString": "Huey"
        }
      ],
      "boundingRectangle": {
        "height": 145.00999450683594,
        "topX": 139,
        "topY": 99.36000061035156,
        "width": 229.00999450683594
      },
      "category": "inkWord",
      "class": "leaf",
      "id": 4,
      "parentId": 3,
      "recognizedText": "They",
      "rotatedBoundingRectangle": [
        {
          "x": 141.39999389648438,
          "y": 94.75
        },
        {
          "x": 376.1700134277344,
          "y": 113.83999633789062
        },
        {
          "x": 365.19000244140625,
          "y": 248.9199981689453
        },
        {
          "x": 130.4199981689453,
          "y": 229.83999633789062
        }
      ],
      "strokeIds": [
        0
      ]
    },
    {
      "alternates": [
        {
          "category": "inkWord",
          "recognizedString": "bow"
        },
        {
          "category": "inkWord",
          "recognizedString": "brow"
        },
        {
          "category": "inkWord",
          "recognizedString": "trow"
        },
        {
          "category": "inkWord",
          "recognizedString": "hour"
        },
        {
          "category": "inkWord",
          "recognizedString": "hew"
        },
        {
          "category": "inkWord",
          "recognizedString": "Crow"
        },
        {
          "category": "inkWord",
          "recognizedString": "tow"
        },
        {
          "category": "inkWord",
          "recognizedString": "hon"
        },
        {
          "category": "inkWord",
          "recognizedString": "frow"
        }
      ],
      "boundingRectangle": {
        "height": 111.01000213623047,
        "topX": 434,
        "topY": 110.36000061035156,
        "width": 161.00999450683594
      },
      "category": "inkWord",
      "class": "leaf",
      "id": 5,
      "parentId": 3,
      "recognizedText": "how",
      "rotatedBoundingRectangle": [
        {
          "x": 438.2900085449219,
          "y": 108.83000183105469
        },
        {
          "x": 599.97998046875,
          "y": 121.9800033569336
        },
        {
          "x": 590.8800048828125,
          "y": 234.02999877929688
        },
        {
          "x": 429.17999267578125,
          "y": 220.8800048828125
        }
      ],
      "strokeIds": [
        1
      ]
    },
    {
      "alternates": [
        {
          "category": "inkWord",
          "recognizedString": "ave"
        },
        {
          "category": "inkWord",
          "recognizedString": "awe"
        },
        {
          "category": "inkWord",
          "recognizedString": "ace"
        },
        {
          "category": "inkWord",
          "recognizedString": "age"
        },
        {
          "category": "inkWord",
          "recognizedString": "Laue"
        },
        {
          "category": "inkWord",
          "recognizedString": "Doue"
        },
        {
          "category": "inkWord",
          "recognizedString": "aye"
        },
        {
          "category": "inkWord",
          "recognizedString": "ate"
        },
        {
          "category": "inkWord",
          "recognizedString": "aver"
        }
      ],
      "boundingRectangle": {
        "height": 41.0099983215332,
        "topX": 699,
        "topY": 188.36000061035156,
        "width": 181.00999450683594
      },
      "category": "inkWord",
      "class": "leaf",
      "id": 6,
      "parentId": 3,
      "recognizedText": "are",
      "rotatedBoundingRectangle": [
        {
          "x": 699.760009765625,
          "y": 184.9600067138672
        },
        {
          "x": 880.780029296875,
          "y": 199.6699981689453
        },
        {
          "x": 877.6099853515625,
          "y": 238.64999389648438
        },
        {
          "x": 696.5900268554688,
          "y": 223.92999267578125
        }
      ],
      "strokeIds": [
        2
      ]
    },
    {
      "alternates": [
        {
          "category": "line",
          "recognizedString": "They bow are"
        },
        {
          "category": "line",
          "recognizedString": "They how ave"
        },
        {
          "category": "line",
          "recognizedString": "They bow ave"
        },
        {
          "category": "line",
          "recognizedString": "Hey how are"
        },
        {
          "category": "line",
          "recognizedString": "They brow are"
        },
        {
          "category": "line",
          "recognizedString": "They how awe"
        },
        {
          "category": "line",
          "recognizedString": "They bow ave"
        },
        {
          "category": "line",
          "recognizedString": "Hey bow are"
        },
        {
          "category": "line",
          "recognizedString": "Hey how ave"
        }
      ],
      "boundingRectangle": {
        "height": 145.00999450683594,
        "topX": 139,
        "topY": 99.36000061035156,
        "width": 741.010009765625
      },
      "category": "line",
      "childIds": [
        4,
        5,
        6
      ],
      "class": "container",
      "id": 3,
      "parentId": 2,
      "recognizedText": "They how are",
      "rotatedBoundingRectangle": [
        {
          "x": 142.2100067138672,
          "y": 84.7699966430664
        },
        {
          "x": 885.2100219726562,
          "y": 145.16000366210938
        },
        {
          "x": 873.4199829101562,
          "y": 290.2300109863281
        },
        {
          "x": 130.4199981689453,
          "y": 229.83999633789062
        }
      ],
      "strokeIds": [
        0,
        1,
        2
      ]
    },
    {
      "boundingRectangle": {
        "height": 145.00999450683594,
        "topX": 139,
        "topY": 99.36000061035156,
        "width": 741.010009765625
      },
      "category": "paragraph",
      "childIds": [
        3
      ],
      "class": "container",
      "id": 2,
      "parentId": 1,
      "rotatedBoundingRectangle": [
        {
          "x": 142.2100067138672,
          "y": 84.7699966430664
        },
        {
          "x": 885.2100219726562,
          "y": 145.16000366210938
        },
        {
          "x": 873.4199829101562,
          "y": 290.2300109863281
        },
        {
          "x": 130.4199981689453,
          "y": 229.83999633789062
        }
      ],
      "strokeIds": [
        0,
        1,
        2
      ]
    },
    {
      "alternates": [
        {
          "category": "inkWord",
          "recognizedString": "You"
        },
        {
          "category": "inkWord",
          "recognizedString": "Lou"
        },
        {
          "category": "inkWord",
          "recognizedString": "your"
        },
        {
          "category": "inkWord",
          "recognizedString": "Your"
        },
        {
          "category": "inkWord",
          "recognizedString": "Tour"
        },
        {
          "category": "inkWord",
          "recognizedString": "Four"
        },
        {
          "category": "inkWord",
          "recognizedString": "Hour"
        },
        {
          "category": "inkWord",
          "recognizedString": "Loue"
        },
        {
          "category": "inkWord",
          "recognizedString": "Lour"
        }
      ],
      "boundingRectangle": {
        "height": 86.01000213623047,
        "topX": 177,
        "topY": 272.3599853515625,
        "width": 194.00999450683594
      },
      "category": "inkWord",
      "class": "leaf",
      "id": 9,
      "parentId": 8,
      "recognizedText": "you",
      "rotatedBoundingRectangle": [
        {
          "x": 177,
          "y": 272.3599853515625
        },
        {
          "x": 373.3699951171875,
          "y": 281.1199951171875
        },
        {
          "x": 369.5899963378906,
          "y": 365.7900085449219
        },
        {
          "x": 173.2100067138672,
          "y": 357.0299987792969
        }
      ],
      "strokeIds": [
        3
      ]
    },
    {
      "alternates": [
        {
          "category": "inkWord",
          "recognizedString": "doing ?"
        },
        {
          "category": "inkWord",
          "recognizedString": "doing??"
        },
        {
          "category": "inkWord",
          "recognizedString": "doing?]"
        },
        {
          "category": "inkWord",
          "recognizedString": "doing]?"
        },
        {
          "category": "inkWord",
          "recognizedString": "dozing ?"
        },
        {
          "category": "inkWord",
          "recognizedString": "dozing?"
        },
        {
          "category": "inkWord",
          "recognizedString": "dosing ?"
        },
        {
          "category": "inkWord",
          "recognizedString": "dosing?"
        },
        {
          "category": "inkWord",
          "recognizedString": "doings ?"
        }
      ],
      "boundingRectangle": {
        "height": 167.00999450683594,
        "topX": 425,
        "topY": 259.3599853515625,
        "width": 408.010009765625
      },
      "category": "inkWord",
      "class": "leaf",
      "id": 10,
      "parentId": 8,
      "recognizedText": "doing?",
      "rotatedBoundingRectangle": [
        {
          "x": 427.5899963378906,
          "y": 257.79998779296875
        },
        {
          "x": 835.4299926757812,
          "y": 271.45001220703125
        },
        {
          "x": 830.0800170898438,
          "y": 431.4100036621094
        },
        {
          "x": 422.239990234375,
          "y": 417.760009765625
        }
      ],
      "strokeIds": [
        5,
        6,
        4
      ]
    },
    {
      "alternates": [
        {
          "category": "line",
          "recognizedString": "you doing ?"
        },
        {
          "category": "line",
          "recognizedString": "You doing ?"
        },
        {
          "category": "line",
          "recognizedString": "You doing?"
        },
        {
          "category": "line",
          "recognizedString": "Lou doing ?"
        },
        {
          "category": "line",
          "recognizedString": "Lou doing?"
        },
        {
          "category": "line",
          "recognizedString": "your doing ?"
        },
        {
          "category": "line",
          "recognizedString": "your doing?"
        },
        {
          "category": "line",
          "recognizedString": "you doing??"
        },
        {
          "category": "line",
          "recognizedString": "Your doing ?"
        }
      ],
      "boundingRectangle": {
        "height": 167.00999450683594,
        "topX": 177,
        "topY": 259.3599853515625,
        "width": 656.010009765625
      },
      "category": "line",
      "childIds": [
        9,
        10
      ],
      "class": "container",
      "id": 8,
      "parentId": 7,
      "recognizedText": "you doing?",
      "rotatedBoundingRectangle": [
        {
          "x": 178.1699981689453,
          "y": 246.14999389648438
        },
        {
          "x": 836.0700073242188,
          "y": 275.5199890136719
        },
        {
          "x": 829.030029296875,
          "y": 433.04998779296875
        },
        {
          "x": 171.1300048828125,
          "y": 403.67999267578125
        }
      ],
      "strokeIds": [
        3,
        5,
        6,
        4
      ]
    },
    {
      "boundingRectangle": {
        "height": 167.00999450683594,
        "topX": 177,
        "topY": 259.3599853515625,
        "width": 656.010009765625
      },
      "category": "paragraph",
      "childIds": [
        8
      ],
      "class": "container",
      "id": 7,
      "parentId": 1,
      "rotatedBoundingRectangle": [
        {
          "x": 178.1699981689453,
          "y": 246.14999389648438
        },
        {
          "x": 836.0700073242188,
          "y": 275.5199890136719
        },
        {
          "x": 829.030029296875,
          "y": 433.04998779296875
        },
        {
          "x": 171.1300048828125,
          "y": 403.67999267578125
        }
      ],
      "strokeIds": [
        3,
        5,
        6,
        4
      ]
    },
    {
      "boundingRectangle": {
        "height": 327.010009765625,
        "topX": 139,
        "topY": 99.36000061035156,
        "width": 741.010009765625
      },
      "category": "writingRegion",
      "childIds": [
        2,
        7
      ],
      "class": "container",
      "id": 1,
      "parentId": 0,
      "rotatedBoundingRectangle": [
        {
          "x": 142.2100067138672,
          "y": 84.7699966430664
        },
        {
          "x": 885.2100219726562,
          "y": 145.16000366210938
        },
        {
          "x": 861.1500244140625,
          "y": 441.1600036621094
        },
        {
          "x": 118.1500015258789,
          "y": 380.7699890136719
        }
      ],
      "strokeIds": [
        0,
        1,
        2,
        3,
        5,
        6,
        4
      ]
    }
  ]
}
