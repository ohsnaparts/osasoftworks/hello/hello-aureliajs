import { Aurelia } from "aurelia-framework";
import { Point } from 'paper';
import InkDetectionService from "components/ink-recognition/ink-recognition-service/InkDetectionService";
import { InkRecognizerResult } from "components/ink-recognition/ink-recognition-service/InkRecognizerResult";
import { Recognition } from "components/ink-recognition/ink-recognition-service/Recognition";
import { RecognitionAlternative } from "components/ink-recognition/ink-recognition-service/RecognitionAlternative";
import { RecognitionUnit } from "components/ink-recognition/ink-recognition-service/RecognitionUnit";


export class SketchPad {
  private lines: string[] = [];
  private recognitions: InkRecognizerResult;
  private inkRecognitionService: InkDetectionService;
  private azureInfos = {
    url: 'https://api.cognitive.microsoft.com/inkrecognizer/v1.0-preview/recognize',
    key: ''
  }

  public onPointsUpdated(event: CustomEvent) {
    var points = event.detail.value;

    this.inkRecognitionService.recognizeInk(
      points, this.azureInfos.url, this.azureInfos.key
    ).then(this.onImageRecogniced.bind(this))
      .catch(reason =>
        console.error(`Something went wrong while trying to interpret the ink: ${reason}`)
      );
  }

  private onImageRecogniced(recognitionResult: InkRecognizerResult) {
    this.lines = this.findRecognizedLines(recognitionResult);
    this.recognitions = recognitionResult;
  }


  private findRecognizedLines = (recognitionResult: InkRecognizerResult): string[] => {
    return !!recognitionResult && !!recognitionResult.recognitionUnits
      ? recognitionResult.recognitionUnits
        .filter(u => u.category === "line") // TODO: enum
        .map(u => u.recognizedText)
      : [];
  }


  constructor() {
    this.inkRecognitionService = new InkDetectionService();
    // TODO: Dependency Injection
  }
}
