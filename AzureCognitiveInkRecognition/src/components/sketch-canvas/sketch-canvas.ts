import { bindable, bindingMode } from "aurelia-framework";
import paper, { view, MouseEvent, Path, Point } from 'paper';


@bindable({ name: 'canvasId', attribute: 'canvas-id', defaultBindingMode: bindingMode.oneTime })
@bindable({ name: 'strokeColor', attribute: 'stroke-color', defaultBindingMode: bindingMode.oneTime })
@bindable({ name: 'strokeSize', attribute: 'stroke-size', defaultBindingMode: bindingMode.oneTime })
@bindable({ name: 'simplifyPaths', attribute: 'simplify-paths', defaultBindingMode: bindingMode.oneTime })
@bindable({ name: 'smoothEdges', attribute: 'smooth-edges', defaultBindingMode: bindingMode.oneTime })
export class SketchCanvas {
  // attributes
  private canvasId: string;
  private strokeColor: string;
  private strokeSize: number;
  private smoothEdges: boolean;
  private simplifyPaths: boolean;

  private paths: Path[] = [];
  private get currentPath() {
    if (!this.paths)
      this.paths = [];

    if (this.paths.length > 0)
      return this.paths[this.paths.length - 1];

    return undefined;
  }
  private set currentPath(path: Path) {
    if (!this.paths)
      this.paths = []

    this.paths.push(path);
  }

  constructor(private element: Element) {
  }

  attached() {
    console.group("Lifecycle hook: 'attached'");

    this.initializePaperCanvas();

    console.groupEnd();
  }


  private initializePaperCanvas() {
    this.paths = [];
    this.setupPaperCanvas(this.canvasId)
      .setupCanvasMouseEvents(view);
  }

  public clearCanvas() {
    this.initializePaperCanvas();
  }


  private setupPaperCanvas(canvasId: string): SketchCanvas {
    console.count(`Setting up paper.js on canvas with id '${this.canvasId}'.`);
    const canvas = document.getElementById(canvasId);
    if (!canvas)
      throw new Error(`Unable to find canvas with specified canvasId '${this.canvasId}'`);

    paper.setup(this.canvasId);

    return this;
  }

  private setupCanvasMouseEvents(view: view): SketchCanvas {
    console.count('Setting up paper.js mouse events.');
    view.onMouseDown = this.handleMouseDown.bind(this);
    view.onMouseDrag = this.handleMouseDrag.bind(this);
    view.onMouseUp = this.handleMouseUp.bind(this);

    return this;
  }


  private createPaperPath(color: string, width: number, enableSkeleton = true): Path {
    console.log('Setting up path.');
    const path = new paper.Path();
    path.strokeColor = color;
    path.strokeWidth = width;
    path.fullySelected = enableSkeleton;

    return path;
  }

  private onStrokeFinished(points: Point[][]) {
    var event = new CustomEvent('on-stroke-finished', {
      detail: { value: points },
      bubbles: true
    });

    this.element.dispatchEvent(event);
  }


  private handleMouseDown(event: MouseEvent) {
    this.currentPath = this.createPaperPath(this.strokeColor, this.strokeSize);
    this.currentPath.moveTo(event.point);
  }

  private handleMouseUp(event: MouseEvent) {

    const points = this.paths.map(p => p.segments.map(s => s.point));
    this.onStrokeFinished(points);

    if (!!this.currentPath) {
      if (this.smoothEdges)
        this.currentPath.smooth();

      if (this.simplifyPaths)
        this.currentPath.simplify(100);
      // Tolerance optional: default 2.5
    }
  }

  private handleMouseDrag(event: MouseEvent) {
    if (!!this.currentPath) {
      this.currentPath.add(event.point);
    }
  }

  private flatten<T>(array: T[][]): T[] {
    return [].concat.apply([], array);
  }
}
