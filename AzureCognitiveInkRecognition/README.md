# Hello-Aurelia

![](./hello-aurelia.gif)

A small [Aurelia](https://aurelia.io) page for playing with  [Paper.js](http://paperjs.org/about/) and [Azure InkRecognizer Cognitive Service](https://azure.microsoft.com/en-us/services/cognitive-services/ink-recognizer/).

```bash
npm install
npm start
```

## Run dev app

Run `au run`, then open `http://localhost:8080`

To open browser automatically, do `au run --open`.

To change dev server port, do `au run --port 8888`.

To enable Webpack Bundle Analyzer, do `au run --analyze`.

To enable hot module reload, do `au run --hmr`.

## Build for production

Run `au build --env prod`.

## Unit tests

Run `au test` (or `au jest`).

To run in watch mode, `au test --watch` or `au jest --watch`.

## Notice

This project is bootstrapped by [aurelia-cli](https://github.com/aurelia/cli).

For more information, go to https://aurelia.io/docs/cli/webpack

